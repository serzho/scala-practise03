seq(webSettings :_*)

name := "scala-practise03"

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

resolvers += "hibernatesqlite-repo" at "https://hibernate-sqlite.googlecode.com/svn/trunk/mavenrepo"

libraryDependencies ++= Seq(
    "org.eclipse.jetty" % "jetty-webapp" % "8.0.1.v20110908" % "container",
    "javax.servlet" % "servlet-api" % "2.5" % "provided",
    "org.scalatest" %% "scalatest" % "1.9.1" % "test",
    "org.hibernate" % "hibernate" % "3.5.4-Final",
    "org.hibernate" % "hibernate-annotations" % "3.5.4-Final",
    "org.hibernate" % "hibernate-entitymanager" % "3.5.4-Final",
    "log4j" % "log4j" % "1.2.17",
    "org.slf4j" % "slf4j-log4j12" % "1.7.4",
    "org.slf4j" % "slf4j-api" % "1.7.4",
    "org.xerial" % "sqlite-jdbc" % "3.7.2",
    "com.applerao" % "hibernatesqlite" % "1.0"
)

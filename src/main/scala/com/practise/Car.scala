package com.practise {

  import javax.persistence._
  import scala.reflect.BeanProperty

  @Entity
  @Table(name = "cars")
  class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @BeanProperty
    var id: Int = _

    @BeanProperty
    var model: String = _

    @BeanProperty
    var number: String = _

    @BeanProperty
    var year: Int = _

    override def toString = id + " " + model + " " + number
  }

  object Car extends ClassPersistable {

    override def getClazz = classOf[Car]
  }
}

package com.practise {

  trait ClassPersistable {

    import javax.persistence._

    def find(id: Integer) = {
      val manager = Persistence.createEntityManagerFactory("default").createEntityManager
      manager find(getClazz, id)
      manager close
    }

    def getClazz: Class[_]
  }
}

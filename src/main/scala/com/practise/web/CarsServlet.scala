package com.practise.web {

  import javax.servlet.http.{HttpServlet,
    HttpServletRequest => HSReq, HttpServletResponse => HSRes}

  import scala.xml._
  import com.practise._

  class CarsServlet extends  HttpServlet {
  
    override def doPost(req: HSReq, res: HSRes) = this.doGet(req, res)

    override def doGet(req: HSReq, res: HSRes) = {
    
      val action = req getParameter ("action")

      val result = action match {
      
        case "index" | null => this.index(req, res)
        case "new"          => this.initialize(req, res)
        case "create"       => this.create(req, res)
        case "edit"         => this.edit(req, res)
        case "update"       => this.update(req, res)
        case "destroy"      => this.destroy(req, res)
        case _              => throw new RuntimeException("action: " + action + " not found")
      }

      res.getWriter().print(result)
    }

    def index(req: HSReq, res: HSRes) = {

      val cars = CarRepository all

      val carsResult = cars map (car => {

            <tr>
              <td>{car.model}</td>
              <td>{car.year}</td>
              <td>{car.number}</td>
              <td>
                <div class="btn-group">
                  <a href={ "/cars?action=edit&id=" + car.id } class="btn btn-mini">Edit</a>
                  <a href={ "/cars?action=destroy&id=" + car.id } class="btn btn-mini btn-danger">Delete</a>
                </div>
              </td>
            </tr>
      }) 
      yieldBody {
        <div>
          <div class="row">
            <h2>Cars</h2>
            <table class="table table-stripped span6">
              <thead>
                <tr class="header">
                  <th>Model</th>
                  <th>Year</th>
                  <th>Number</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {carsResult}
              </tbody>
            </table>
          </div>
          <a href="/cars?action=new" class="btn btn-primary">Add Car</a>
        </div>
      }
    }

    def initialize(req: HSReq, res: HSRes) = { 

      yieldBody {
      
        <div>
          <h1>New Car</h1>
          <form action="/cars" method="post">

            <input type="hidden" name="action" value="create" />

            <label for="model">Model</label>
            <input type="text" name="model" required="required"/>

            <br />
            <label for="number">Number</label>
            <input type="text" name="number" />

            <br />
            <label for="year">Year</label>
            <input type="number" name="year" />

            <br />
            <input type="submit" />
          </form>
        </div>
      }
    }

    def create(req: HSReq, res: HSRes) = {
      
      val model = req getParameter ("model")
      val number = req getParameter ("number")
      val year = req getParameter ("year") toInt

      val car = new Car
      car.setModel(model)
      car.setNumber(number)
      car.setYear(year)

      CarRepository save (car)

      this index (req, res)
    }

    def edit(req: HSReq, res: HSRes) = {

      val id = req getParameter ("id")
      val car = CarRepository.find(id.toInt).get

      yieldBody {

        <div>
          <h1>Edit Car</h1>
          <form action="/cars" method="post">

            <input type="hidden" name="action" value="update" />

            <input type="hidden" name="id" value={car.id.toString} />

            <label for="model">Model</label>
            <input type="text" name="model" required="required" value={car model} />

            <br />
            <label for="number">Number</label>
            <input type="text" name="number" value={car number} />

            <br />
            <label for="year">Year</label>
            <input type="text" name="year" value={car.year.toString} />

            <br />
            <input type="submit" />
          </form>
        </div>
      }
    }

    def update(req: HSReq, res: HSRes) = {
    
      val id = req getParameter ("id")
      val model = req getParameter ("model")
      val number = req getParameter ("number")
      val year = req getParameter ("year") toInt

      val car = CarRepository.find(id.toInt).get
      car.model = model
      car.number = number
      car.year = year

      CarRepository save (car)

      this index (req, res)
    }

    def destroy(req: HSReq, res: HSRes) = {

      val id = req getParameter ("id")
      val car = CarRepository.find(id.toInt).get

      CarRepository.delete(car)

      this index (req, res)
    }

    private def yieldBody(body: => Node) =
        <html>
          <head>
            {css}
            <title>Cars</title>
          </head>
          <body>
            <div class="container">
              {body}
            </div>
          </body>
        </html>

    private def css =
      <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet" />
  }
}

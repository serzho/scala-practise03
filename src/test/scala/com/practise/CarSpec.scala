package com.practise {

  import org.scalatest.FlatSpec
  import org.scalatest.BeforeAndAfterEach
  import org.scalatest.matchers.ShouldMatchers

  import javax.persistence._

  class CarSpec extends FlatSpec with ShouldMatchers with BeforeAndAfterEach {

    var car: Car = _

    override def beforeEach() {
      CarRepository.deleteAll
      car = new Car
      car.setModel("Seat")
      car.setNumber("RF1829")
      car.setYear(2008)
      CarRepository.save(car)
    }

    it should "save car and retrieve it" in {
      car = CarRepository.find(car.getId).head
      car.getModel should be("Seat")
    }

    it should "update car" in {
      car = CarRepository.find(car.getId).head
      car.setModel("BMW")
      CarRepository.save(car)
      car = CarRepository.find(car.getId).head
      car.getModel should be("BMW")
    }

    it should "delete car" in {
      CarRepository.delete(car)
      CarRepository.find(car.getId) should be(None)
    }

    it should "select all cars" in {
      CarRepository.all.size should not be(0)
    }

    it should "find cars by name" in {
      val otherCar = new Car
      otherCar.setModel("Seat")
      otherCar.setNumber("FD1298")
      otherCar.setYear(2009)
      CarRepository.save(otherCar)
      
      val cars = CarRepository.findByModel("Seat")

      cars.size should not be(0)
      cars.foreach { _.getModel() should be("Seat") }
    }

    it should "find cars by year" in {
      val otherCar = new Car
      otherCar.setModel("BMW")
      otherCar.setNumber("FD1298")
      otherCar.setYear(2008)
      CarRepository.save(otherCar)
      
      val cars = CarRepository.findByYear(2008)

      cars.size should not be(0)
      cars.foreach { _.getYear() should be(2008) }
    }

    it should "find car by number" in {
      val car = CarRepository.findByNumber("RF1829").head
      car.getNumber() should be("RF1829")
    }
  }
}

